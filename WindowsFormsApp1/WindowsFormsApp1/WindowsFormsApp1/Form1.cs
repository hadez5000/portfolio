﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.IO;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApp1
{

    public partial class Form1 : Form
    {
        string currentword;
        char[] guess;
        char[] comparechars;
        int live = 8;
        StartDAPI APIdictionary;
        List<string> wordsfound = new List<string>();

        public Form1()
        {
            InitializeComponent();
            word.BaseAddress = new Uri("http://www.schmipsum.com");
            dictionary.BaseAddress = new Uri("https://dictionary.yandex.net/");
        }

        HttpClient word = new HttpClient();

        public async Task<wordAPI> GetIPSUMasync(string path)
        {
            wordAPI product = null;
            HttpResponseMessage response = await word.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                product = await response.Content.ReadAsAsync<wordAPI>();
            }
            return product;

        }
        HttpClient dictionary = new HttpClient();

        public async Task<StartDAPI> GetdicAsync(string path)
        {
            StartDAPI dict = null;
            HttpResponseMessage dictio = await dictionary.GetAsync(path);
            if (dictio.IsSuccessStatusCode)
            {
                dict = await dictio.Content.ReadAsAsync<StartDAPI>();
            }
            return dict;
        }
        public async void Start_Click(object sender, EventArgs e)
        {

            this.Congratsbox.Text = " ";
            this.textBox1.Text = " ";
            textBox2.Text = " ";
            textBox3.Text = " ";

            if (new Ping().Send("www.google.nl").Status == IPStatus.Success)
            {
                word.DefaultRequestHeaders.Accept.Clear();
                word.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                dictionary.DefaultRequestHeaders.Accept.Clear();
                dictionary.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
               
                wordAPI APIword = await GetIPSUMasync("/ipsum/shakespeare/20");
                string stringapi = APIword.ToString().ToLower();
                string[] words = stringapi.Split(new char[] { ' ', ',', '\n', '\t', ':', ';', '.', '}', '{', '[', ']', '?', '"', '/', '-', '"', '(', ')', '|', '!', '\'' });
                Array.Sort(words, (a, b) => b.Length - a.Length);
                currentword = words[0]; //zonder dit is allah akbar
                wordsfound.Add(currentword);
                guess = new char[currentword.Length];
                for (int p = 0; p < currentword.Length; p++)
                    guess[p] = '_';
                live = 8;
                this.Wordbox.Text = string.Join(" ", guess);
                this.Lives.Text = live.ToString();
                panel1.Invalidate();
                panel1.BackgroundImage = null;
                APIdictionary = await GetdicAsync("/api/v1/dicservice.json/lookup?key=dict.1.1.20170713T144811Z.5839395825c6806a.8bdaa85adda7b2698f17ec3a1bbe966d25ce954f&lang=en-en&text=" + currentword);

            }
            else
            {
                var filecontent = File.ReadAllLines("Alreadyfoundwords.txt");
                List<string> foundWords = new List<string>();
                foreach (string item in filecontent)
                {
                    foundWords.Add(item);
                }
                Random rand = new Random();
                int random = rand.Next(foundWords.Count);
                currentword = foundWords[random]; //zonder dit is allah akbar
                guess = new char[currentword.Length];
                for (int p = 0; p < currentword.Length; p++)
                    guess[p] = '_';
                live = 8;
                this.Wordbox.Text = string.Join(" ", guess);
                this.Lives.Text = live.ToString();
                panel1.Invalidate();
                panel1.BackgroundImage = null;

            }
        }
       
        public void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {

            char a = e.KeyChar;
            int index = textBox1.Text.IndexOf(e.KeyChar);
            if (index > -1)
            {
                return;
            }

            if (!char.IsLetter(a))
            {
                return;
            }
            if (live > 0)
                if (Congratsbox.Text != "Congratulations you have won!")
                {
                    textBox1.Text = textBox1.Text + e.KeyChar + ',';
                }
                else
                {
                    return;
                }

            if (live == 0)
            {
                panel1.Invalidate();
                panel1.BackgroundImage = Properties.Resources.Skull;
                //return;"C:\imagetest\Bill.jpg"
            }
            else
            {
                bool found = false;

                this.Congratsbox.Text = " ";
                comparechars = currentword.ToCharArray();
                for (int A = 0; A < currentword.Length; A++)
                {
                    if (comparechars[A] == a)
                    {
                        guess[A] = a;
                        this.Wordbox.Text = string.Join(" ", guess);
                        found = true;
                    }
                }

                if (found)
                {
                    this.Congratsbox.Text = "you found a letter!";

                }
                else
                {
                    Congratsbox.Text = "you did not find a letter!";
                    live--;
                    Lives.Text = live.ToString();
                    panel1.Refresh();
                }
                if (live == 0)
                {
                    this.Congratsbox.Text = "Game Over!";
                    this.Wordbox.Text = currentword;
                    panel1.Invalidate();
                    panel1.BackgroundImage = Properties.Resources.Skull;
                    if (new Ping().Send("www.google.nl").Status == IPStatus.Success)
                    {
                        if (APIdictionary.def.Length == 0)

                        {
                            return;
                        }

                        textBox2.Text = APIdictionary.def[0].pos + ':' + APIdictionary.def[0].text;
                        textBox3.Text = "meaning:" + APIdictionary.def[0].tr[0];
                    }
                }
                for (int g = 0; g < guess.Length; g++)
                {
                    if (guess[g] == '_')
                    {
                        return;
                    }

                }
                Congratsbox.Text = "Congratulations you have won!";
                if (Congratsbox.Text == "Congratulations you have won!")
                {
                    panel1.Invalidate();

                    live = 8;
                    if (live == 8)
                    {
                        panel1.BackgroundImage = Properties.Resources.Balloon;
                    }
                    if (new Ping().Send("www.google.nl").Status == IPStatus.Success)
                    {
                        if (APIdictionary.def.Length == 0)

                        {
                            return;
                        }

                        textBox2.Text = APIdictionary.def[0].pos + ':' + APIdictionary.def[0].text;
                        textBox3.Text = "meaning:" + APIdictionary.def[0].tr[0];
                    }
                }
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (live == 0)
            {
                return;
            }
            if (live < 8)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 85, 190, 210, 190);
            }
            if (live < 7)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 148, 190, 148, 50);
            }
            if (live < 6)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 148, 50, 198, 50);
            }
            if (live < 5)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 198, 50, 198, 70);
            }
            if (live < 4)
            {
                e.Graphics.DrawEllipse(new Pen(Color.Black, 2), new Rectangle(188, 70, 20, 20));
            }
            if (live < 3)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 198, 90, 198, 130);
            }
            if (live < 2)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 198, 95, 183, 115);
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 198, 95, 213, 115);
            }
            if (live < 1)
            {
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 198, 130, 213, 170);
                e.Graphics.DrawLine(new Pen(Color.Black, 2), 198, 130, 183, 170);
            }

        }
        private void Form1_Load(object sender, EventArgs e)
        {
            panel1.BackgroundImage = null;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Are you sure you want to exit?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
               Application.Exit();
            else
            {
                return;
            }

        }
    }
}