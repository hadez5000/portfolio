
import vue from 'vue';
import './style.scss';

import vueResource from 'vue-resource';
vue.use(vueResource);

import moment from 'moment-timezone';
moment.tz.setDefault("UTC");
Object.defineProperty(vue.prototype, '$moment', { get() { return this.$root.moment} })


import {checkFilter, setDay} from './util/bus'
const bus = new vue();
Object.defineProperty(vue.prototype, '$bus', { get() {return this.$root.bus } })

import VueRouter from 'vue-router';
vue.use(VueRouter);

import routes from './util/routes'
const router = new VueRouter({routes})

import Tooltip from './util/tooltip';
vue.use(Tooltip);

new vue({
	el: '#app',
	data: {
		genre: [],
		time: [],
		movies: [],
		moment,
		day: moment(),
		bus
	}, 
	created(){
		this.$http.get('/api').then(response => {
			this.movies = response.data;
		});
		this.$bus.$on('check-filter', checkFilter.bind(this));
		this.$bus.$on('set-day', setDay.bind(this))
	},
	router
});

/*import {addClass, removeClass} from './util/helpers'

let mouseOutHandler = function(event) {
	let span = event.target.parentNode.getElementsByTagName('SPAN')[0]
	removeClass(span, 'tooltip-show')
};
let mouseOverHandler = function(event) {
	let span = event.target.parentNode.getElementsByTagName('SPAN')[0]
	addClass(span, 'tooltip-show')
	
};

vue.directive('tooltip', {
	bind(el, bindings) {
		let span = document.createElement('SPAN');
		let text = document.createTextNode(`Seats available: ${bindings.value.seats}`);
		span.appendChild(text);
		addClass(span, 'tooltip')
		el.appendChild(span);
		let div = el.getElementsByTagName('DIV')[0]
		div.addEventListener('mouseover', mouseOverHandler)
		div.addEventListener('mouseout', mouseOutHandler)
		}
	}
);*/
