import overview from '../components/Overview.vue'
import detail from '../components/Detail.vue'

export default[
	{path: '/', component: overview , name: 'home'},
	{path: '/movie/:id', component:detail , name:'movie'},
	{path: '*', redirect: {name : 'home'}}
];