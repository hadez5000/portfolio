import Vue from 'vue'
import vuex from 'vuex'
import axios from 'axios'

import moment from 'moment-timezone'
moment.tz.setDefault('UTC');


Vue.use(vuex)
export default new vuex.Store({
  	state: {
  		currentMonth: 12,
  		currentYear: 2017,
  		eventformPosX: 0,
  		eventformPosY: 0,
  		eventformActive: false,
  		eventformDate: moment(),
  		events:[
  			
			]
  },
  mutations: {
  	SetCurrentMonth(state, payload) {
  		state.currentMonth = payload;
  	},
  	SetCurrentYear(state, payload) {
  		state.currentYear = payload
  	},
  	eventformPos(state, payload){
  		state.eventformPosX = payload.x;
  		state.eventformPosY = payload.y;
  	},
  	eventformActive(state,payload) {
  		state.eventformActive = payload;
  	},
  	addEvent(state, payload) {
  		state.events.push(payload)
  	},
  	eventformDate(state,payload) {
  		state.eventformDate = payload;
  	},
  },
  	actions: {
  	 addEvent(context, payload) {
  	 	return new Promise((resolve, reject) => {
  	 		let obj = {
  			description: payload,
  			date: context.state.eventformDate,
  		}
  		
  		axios.post('/add_event', obj).then(response => {
  			if (response.status === 200) {
  				context.commit('addEvent', obj)
  				resolve()
  			}
  			else {
  				alert('Something went wrong, please try again')
  				reject()
  			}
  		})
  	 	})
  	 	
  	 }
  },
})
