let PRICE = 9.99;
let LOADNUM = 10;
new Vue({
    el: '#app',
    data: {
        total: 0,
        items: [],
        cart: [],
        results: [],

        search: 'anime',
        lastSearch: 'anime',
        loading: false,
        price: PRICE,
    },
    methods: {
        appendItems: function(){
            if(this.items.length < this.results.length){
               var append = this.results.splice(this.items.length, this.items.length + LOADNUM)
               this.items = this.items.concat(append)
            }
        },

        onSubmit: function () {
            if (this.search === '')
            {
               alert('You must enter a string') 
            }
            else{
                this.items = [];
            this.loading = true;
            this.$http.get('/search/'.concat(this.search))
                .then(function (res) {
                    this.results = res.data;
                    this.appendItems();
                    this.lastSearch = this.search;
                    this.loading = false;
                });
            }
            
            },
        addItem: function (index) {
            this.total += PRICE;
            let item = this.items[index];
            let found = false;
            for (var i = 0; i < this.cart.length; i++) {
                if (this.cart[i].id === item.id) {
                    this.cart[i].qty++;
                    found = true;
                    break;
                    }
                }
            if (!found)
            {
                this.cart.push({
                    id: item.id,
                    title: item.title,
                    qty: 1,
                    price: PRICE
                });
            }
        },
        inc: function (item) {
            item.qty++;
            this.total += PRICE;
        },
        dec: function (item) {
            if (item.qty <= 1)
            {
                var index = this.cart.indexOf(item.id)
                this.cart.splice(index, 1)
                this.total -= PRICE;
            }
            else
            {
                item.qty--;
                this.total -= PRICE;
            }
            
        },
    },
    filters: {
        currency: function (price) {
            return '$'.concat(price.toFixed(2));
        }
    },
    mounted: function() {
        this.onSubmit();
        var vueInstance = this;
        var elem = document.getElementById("product-list-bottom")
        var watcher = scrollMonitor.create(elem);
        watcher.enterViewport(function(){
        vueInstance.appendItems();
})
    }
});

