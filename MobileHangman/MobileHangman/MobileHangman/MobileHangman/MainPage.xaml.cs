﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using System.Net.Http.Headers;

namespace MobileHangman
{
    public partial class MainPage : ContentPage
    {
        string KeyPressed = "";
        bool changeletter = true;
        string currentword;
        char[] GuessWords;
        char[] comparechars;
        string currentletters = "";
        int live = 8;
        bool startup = true;
        DictionaryAPIHierarchy APIdictionary;
        HttpClient WordIpsum = new HttpClient();
        HttpClient Dictionary = new HttpClient();
        char[] delimiters = { ' ', ',', '\n', '\t', ':', ';', '.', '}', '{', '[', ']', '?', '"', '/', '-', '"', '(', ')', '|', '!', '\'', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };

        public MainPage()
        {
            InitializeComponent();
            WordIpsum.BaseAddress = new Uri("http://www.schmipsum.com");
            Dictionary.BaseAddress = new Uri("https://dictionary.yandex.net/");
        }
        #region AsyncTasks
        public async Task<GetWordAPI> GetIpsumWordAsync(string path)
        {
            GetWordAPI Product = null;
            HttpResponseMessage response = await WordIpsum.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                Product = await response.Content.ReadAsAsync<GetWordAPI>();
            }
            return Product;
        }
        public async Task<DictionaryAPIHierarchy> GetDictionaryAsync(string path)
        {
            DictionaryAPIHierarchy Product = null;
            HttpResponseMessage response = await Dictionary.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                Product = await response.Content.ReadAsAsync<DictionaryAPIHierarchy>();
            }
            return Product;
        }

        #endregion

        private async void StartButton_Clicked(object sender, EventArgs e)
        {
            BackgroundImage = null;
            WordIpsum.DefaultRequestHeaders.Accept.Clear();
            WordIpsum.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            Dictionary.DefaultRequestHeaders.Accept.Clear();
            Dictionary.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            first.Text = "";
            second.Text = "";
            startup = false;

            GetWordAPI APIWord = await GetIpsumWordAsync("/ipsum/shakespeare/5");
            string ApiWord = APIWord.ToString().ToLower();
            string[] words = ApiWord.Split(delimiters);
            Array.Sort(words, (a, b) => b.Length - a.Length);
            currentword = words[0];
            GuessWords = new char[currentword.Length];
            for (int p = 0; p < currentword.Length; p++)
            {
                GuessWords[p] = '_';
            }
            live = 8;
            Word.Text = string.Join(" ", GuessWords);
            Lives.Text = live.ToString();
            kb.Focus();
            currentletters = "";
            LettersUsed.Text = "";
            APIdictionary = await GetDictionaryAsync("/api/v1/dicservice.json/lookup?key=dict.1.1.20170713T144811Z.5839395825c6806a.8bdaa85adda7b2698f17ec3a1bbe966d25ce954f&lang=en-en&text=" + currentword);
        }
        private void kb_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (changeletter && !startup)
            {
                KeyPressed = e.NewTextValue;
                program();
            }
            else
                changeletter = true;
        }
        public void program()
        {
            changeletter = false;
            kb.Text = "";
            char a = KeyPressed[0];
            if (!char.IsLetter(a))
                return;
            if (currentletters.Contains(KeyPressed))
                return;
            currentletters = string.Format("{0}, {1}", currentletters, KeyPressed);
            LettersUsed.Text = currentletters;
            bool found = false;
            comparechars = currentword.ToCharArray();
           
            for (int A = 0; A < currentword.Length; A++)
            {
                if (comparechars[A] == a)
                {
                    GuessWords[A] = a;
                    Word.Text = string.Join(" ", GuessWords);
                    found = true;
                }
            }
            if (found)
                message.Text = "You found a letter!";
            else
            {
                message.Text = "You did not find a letter!";
                live--;
                Lives.Text = live.ToString();
            }
            if (live > 0 && !GuessWords.Contains('_'))
            {
                message.Text = "You Won! To play again press start!";
                BackgroundImage = "Balloon.jpg";
                kb.Unfocus();
                DictWords();

            }
            else if (live == 0 && GuessWords.Contains('_'))
            {
                message.Text = "You lost. To play again press start!";
                Word.Text = currentword;
                BackgroundImage = "Head_skull_anterior_view.jpg";
                kb.Unfocus();
                DictWords();
            }
        }
        public void DictWords()
        {
            if (APIdictionary.def.Length == 0)
            {
                return;
            }

            first.Text = APIdictionary.def[0].pos + ':' + APIdictionary.def[0].text;
            second.Text = "meaning:" + APIdictionary.def[0].tr[0];
        }
    }
}
